class PersonelManager {
  constructor() {
    this.ListPerson = [];
  }
  addPerson(person) {
    this.ListPerson = [...this.ListPerson, person];
  }
  removePeron(ma) {
    let index = this.ListPerson.findIndex((person) => {
      return person.ma == ma;
    });
    this.ListPerson.splice(index, 1);
  }
  updatePerson(ma) {
    let index = this.ListPerson.findIndex((person) => {
      return person.ma == ma;
    });
    return index;
  }
}
